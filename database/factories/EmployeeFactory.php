<?php

use Faker\Generator as Faker;

$factory->define(App\Employee::class, function (Faker $faker) {
    return [
        'name' => $faker->text(10),
        'profession' => $faker->text(15),
    ];
});
