<?php

use Illuminate\Http\Request;


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::middleware('cors')->group(function () {
    Route::get('employees', 'EmployeeController@index');

    Route::get('employee/{id}', 'EmployeeController@show');

    Route::post('employee', 'EmployeeController@store');

    Route::put('employee/{id}', 'EmployeeController@update');

    Route::delete('employee/{id}', 'EmployeeController@destroy');
});


