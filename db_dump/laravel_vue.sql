-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 11, 2019 at 04:55 AM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel_vue`
--

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profession` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `name`, `profession`, `created_at`, `updated_at`) VALUES
(1, 'Eos.', 'Reiciendis.', '2019-11-10 09:31:33', '2019-11-10 09:31:33'),
(3, 'Molestiae.', 'Suscipit.', '2019-11-10 09:31:33', '2019-11-10 09:31:33'),
(4, 'Updated name', 'Updated profession', '2019-11-10 09:31:33', '2019-11-10 10:47:47'),
(5, 'Est est.', 'Sint vitae.', '2019-11-10 09:31:33', '2019-11-10 09:31:33'),
(6, 'Et alias.', 'Mollitia eum.', '2019-11-10 09:31:33', '2019-11-10 09:31:33'),
(7, 'Tempore.', 'Ut excepturi.', '2019-11-10 09:31:33', '2019-11-10 09:31:33'),
(8, 'Eligendi.', 'Sapiente quia.', '2019-11-10 09:31:33', '2019-11-10 09:31:33'),
(9, 'Est totam.', 'Est inventore.', '2019-11-10 09:31:33', '2019-11-10 09:31:33'),
(10, 'Ut.', 'Id ullam.', '2019-11-10 09:31:33', '2019-11-10 09:31:33'),
(11, 'Quia.', 'Consequatur.', '2019-11-10 09:31:33', '2019-11-10 09:31:33'),
(12, 'Aut.', 'Culpa dolore.', '2019-11-10 09:31:33', '2019-11-10 09:31:33'),
(13, 'Molestias.', 'Quis et.', '2019-11-10 09:31:33', '2019-11-10 09:31:33'),
(14, 'Id.', 'Magni eum non.', '2019-11-10 09:31:33', '2019-11-10 09:31:33'),
(15, 'Iure.', 'Ipsum.', '2019-11-10 09:31:33', '2019-11-10 09:31:33'),
(16, 'Eos.', 'Odit cum.', '2019-11-10 09:31:33', '2019-11-10 09:31:33'),
(17, 'Non iure.', 'Eum aperiam.', '2019-11-10 09:31:33', '2019-11-10 09:31:33'),
(19, 'Sint.', 'Ut molestiae.', '2019-11-10 09:31:34', '2019-11-10 09:31:34'),
(20, 'Velit hic.', 'Voluptas.', '2019-11-10 09:31:34', '2019-11-10 09:31:34'),
(21, 'Maxime.', 'Voluptatem.', '2019-11-10 09:31:34', '2019-11-10 09:31:34'),
(23, 'Dolores.', 'Sed odio.', '2019-11-10 09:31:34', '2019-11-10 09:31:34'),
(24, 'Updated name', 'Updated profession', '2019-11-10 09:31:34', '2019-11-10 15:04:07'),
(25, 'Aperiam.', 'Velit sapiente.', '2019-11-10 09:31:34', '2019-11-10 09:31:34'),
(28, 'Laborum.', 'Ea ratione.', '2019-11-10 09:31:34', '2019-11-10 09:31:34'),
(31, 'The Employee', 'Markerter.', '2019-11-10 10:44:07', '2019-11-10 20:19:23');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_11_10_100346_create_employees_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
